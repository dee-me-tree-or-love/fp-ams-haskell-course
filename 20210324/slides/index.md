# Haskell: From Beginner to Intermediate #7

## Functors, Functor Laws

### FP AMS 24/03/2021 

---

# Goal

![inline](learning-curve.png)

---

# Overall Planning

1. Basics
2. **Functors, Applicative Functors**, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)

---

# Last Week:

## [fit] Functors
  
---

# This Week:

# [fit] More
# [fit] Functors

---

# What Is a Functor Again?

Broadly speaking:

> "A functor is something that can be mapped over"

In Haskell:

```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b
```

Partially applied:

```haskell
  fmap :: (a -> b) -> (f a -> f b)
```

---

# Homework (1/4)

Make `(->) r` an instance of `Functor`

> "what a mindfrack"
-- Tjeerd Hans

Hints:

- `(->) r` is a type constructor for all unary functions that take a value of
type `r` as their argument
- Try to come up with the type of `fmap` for this instance first, 
  `fmap :: (a -> b) -> ... -> ...`
  
---

# Homework (2/4)

```haskell
instance Functor ((->) r) where
  fmap :: (a -> b) -> f a -> f b
```

---

# Homework (2/4)

```haskell
instance Functor ((->) r) where
  fmap :: (a -> b) -> (r -> a) -> f b
```

---
# Homework (2/4)

```haskell
instance Functor ((->) r) where
  fmap :: (a -> b) -> (r -> a) -> (r -> b)
```

---

# Homework (3/4)

```haskell
instance Functor ((->) a) where
  fmap :: (b -> c) -> (a -> b) -> (a -> c)
```

---

# Homework (4/4)

```haskell
instance Functor ((->) r) where
  fmap :: (a -> b) -> (r -> a) -> (r -> b)
  fmap = (.)
```

---

![left](neil-as-vyvyan.jpg)

# [fit] Laws?

---

# Functor Laws

## First Law

```haskell
fmap id = id
```

Remember:

```haskell
id :: a -> a
id x = x
```

---

# Functor Laws

## First Law

```haskell
fmap id = id

instance Functor Maybe where
  fmap _ Nothing  = Nothing
  fmap f (Just x) = Just (f x)
  
fmap id Nothing = Nothing 
id Nothing      = Nothing

fmap id (Just x) = Just (id x) = Just x
id (Just x)                    = Just x
```

_**Homework**_: do this for `Either a` and `[]`

---

# Functor Laws

## Second Law

```haskell
fmap (f . g) = fmap f . fmap g
```

Remember:

```haskell
(.) :: (b -> c) -> (a -> b) -> a -> c
(f . g) x = f (g x)
```
---

# Functor Laws

## Second Law

```haskell
fmap (f . g) = fmap f . fmap g

instance Functor Maybe where
  fmap _ Nothing  = Nothing
  fmap f (Just x) = Just (f x)
  
fmap (f . g) Nothing = Nothing
(fmap f . fmap g) Nothing = fmap f (fmap g Nothing) = fmap f Nothing = Nothing

fmap (f . g) (Just x) = Just ((f . g) x) = Just (f (g x))
fmap f (fmap g (Just x)) = fmap f (Just (g x)) = Just (f (g x))
```

_**Homework**_: again, do this for `Either a` and `[]`

---

![left](beaker.jpg)

# What happens if you break these laws?

```haskell
data CMaybe a 
  = CNothing 
  | CJust Int a 
  deriving (Show) 

instance Functor CMaybe where
  fmap f CNothing = 
    CNothing
  fmap f (CJust counter x) = 
    CJust (counter+1) (f x)
```

_**Homework**_: show that for this instance the functor laws do not hold
