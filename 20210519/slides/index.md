# Haskell: From Beginner to Intermediate #13

## Writing a JSON Library - Part 2

### FP AMS 19/05/2021

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50 %](real-world-haskell.jpg)![inline 60%](book-of-monads.png)

---

# Writing a JSON Library

## Planning

1. Introduction
1. Representing JSON data:
   1. data type
   1. accesor functions
1. **Output: pretty printing JSON data**
1. Input: parsing JSON data
1. Aeson

---

# Representing JSON Data

```haskell
data JValue = JString String
            | JNumber Double
            | JBool Bool
            | JNull
            | JObject [(String, JValue)]
            | JArray [JValue]
              deriving (Eq, Ord, Show)
```

---

# Combinator Pattern

- some type `T`
- functions for constructing "primitive" values of type `T`
- combinators which can combine values of type `T` in various ways to build up more complex values of type `T`
