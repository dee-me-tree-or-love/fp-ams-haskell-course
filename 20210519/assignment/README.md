# JSON Assignment #1

Last session we looked at the following data type to represent JSON data:

``` haskell
data JValue
  = JString String
  | JNumber Double
  | JBool Bool
  | JNull
  | JObject [(String, JValue)]
  | JArray [JValue]
  deriving (Eq, Ord, Show)
```

We also defined several accessors to extract data from `JValue`s, but these
accessors are a bit difficult to use. The same goes for the value constructors.
To solve this problem we introduce the following typeclass:

``` haskell
class JSON a where
  toJValue :: a -> JValue
  fromJValue :: JValue -> Either JSONError a
```

Where `JSONError` is a type synonym for `String`.

The assignment is to implement several instances of this typeclass. Use the
[JSON module](src/JSON.hs) provided as a starting point.
